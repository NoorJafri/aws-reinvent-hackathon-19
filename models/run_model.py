from sagemaker.tensorflow.serving import Model

def deploy_model(model_data, role):
    model = Model(model_data=model_data, role=role)
    predictor = model.deploy(initial_instance_count=1, instance_type='ml.c5.xlarge', accelerator_type='ml.eia1.medium')


def run_model(predictor):
    input = {
  'instances': [1.0, 2.0, 5.0]
    }
    result = predictor.predict(input)